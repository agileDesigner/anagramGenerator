import random
from pathlib import Path

def isFile(filePath):
    path = Path(filePath)
    return path.is_file()

def writeDefaultSettingsFile():
    with open("settings", "w") as settingsFile:
        settingsFile.write("1\n1")

def checkSettingsFile():
    if not isFile("settings"):
        writeDefaultSettingsFile()

def readSettings(settingsNo):
    checkSettingsFile()
    with open("settings", "r") as settingsFile:
        try:
            settingsOption = int(settingsFile.read().split("\n")[settingsNo])
        except Exception:
            print("An issue with the settings file was detected, returning it to its default values")
            writeDefaultSettingsFile()
            settingsOption = 1
    return settingsOption

def writeSettings(settings):
    with open("settings", "w") as settingsFile:
        settingsFile.writelines("%s\n" % str(s) for s in settings)

def settingsThreeOptions(title, options):
    print(title)
    for i in range(3):
        print(f"{i+1}. {options[i]}")
    while True:
        settingsSelect = input("Enter the number of the setting option you'd like: ")
        if settingsSelect.isdigit() and int(settingsSelect) > 0 and int(settingsSelect) <= 3:
            return int(settingsSelect)-1
        else:
            print("Incorrect input (Enter 1-3)")

def settings():
    formatOptions = ("no formatting","all uppercase","all lowercase")
    spaceOptions = ("keep words seperate", "keep spaces in same place", "treat spaces like all other characters")
    while True:
        settingsList = []
        settingsList.append(readSettings(0))
        settingsList.append(readSettings(1))
        print(f"Current settings\n1. Format phrase: {formatOptions[settingsList[0]]}\n2. Space handling: {spaceOptions[settingsList[1]]}")
        settingsInput = input("Enter the number of what you'd like to change or 'q' to quit the settings: ").lower()
        if settingsInput == "q":
            break;
        elif settingsInput == "1":
            settingsList[0] = settingsThreeOptions("Format options", formatOptions) 
        elif settingsInput == "2":
            settingsList[1] = settingsThreeOptions("Space handling", spaceOptions)
        writeSettings(settingsList)

def applyFormatSetting(inputText):
    formatSetting = readSettings(0)
    if formatSetting == 0:
        return inputText
    elif formatSetting == 1:
        return inputText.upper()
    elif formatSetting == 2:
        return inputText.lower()
    else:
        print("An issue with the settings file was detected, returning it to its default values")
        writeDefaultSettingsFile()
        return inputText.upper()

def generate(inputText):
    inputLen = len(inputText)
    anagram = ""
    for character in range(0, inputLen):
        charTake = random.randint(0, inputLen-1-character)
        anagram += inputText[charTake]
        inputText = inputText[:charTake] + inputText[charTake + 1:]
    return anagram

def keepSeperate(inputText):
    wordList = inputText.split(" ")
    anagram = ""
    for word in wordList:
        anagram += " " + generate(word)
    return anagram.strip(" ")

def keepSpaces(inputText):
    spacesNos = []
    for charNo in range(len(inputText)-1, -1, -1):
        if inputText[charNo] == ' ':
            spacesNos.append(charNo)
            inputText = inputText[:charNo] + inputText[charNo + 1:]
    anagram = generate(inputText)
    for charNo in spacesNos:
        anagram = anagram[:charNo] + ' ' + anagram[charNo:]
    return anagram

def generateFull(inputText):
    keepSeperateSetting = readSettings(1)
    if keepSeperateSetting == 0:
        return keepSeperate(inputText)
    elif keepSeperateSetting == 1:
        return keepSpaces(inputText)
    elif keepSeperateSetting == 2:
        return generate(inputText)
    else:
        print("An issue with the settings file was detected, returning it to its default values")
        writeDefaultSettingsFile()
        return keepSpaces(inputText)

while True:
    phrase = input("Enter your word, phrase, '-s' for settings or '-q' to quit: ").strip()
    if phrase == "-q":
        break;
    elif phrase == "-s":
        settings()
    else:
        anagram = generateFull(applyFormatSetting(phrase))
        print(anagram)
